package fade

import "fmt"

const in = "in"
const out = "out"

type Options struct {
	FadeType string  `json:"type"`
	Start    float64 `json:"start"`
	Duration float64 `json:"duration"`
	Color    string  `json:"color"`
}

func (o Options) String() string {
	return buildFilter(o)
}

func NewOptions() Options {
	return Options{
		FadeType: in,
		Start:    0,
		Duration: 0,
		Color:    "black",
	}
}

func FadeTypeArg(fadeType string) string {
	switch fadeType {
	case in:
		return fmt.Sprintf("t=%s", in)
	case out:
		return fmt.Sprintf("t=%s", out)
	default:
		return fmt.Sprintf("t=%s", in)
	}
}

func StartArg(start float64) string {
	return fmt.Sprintf("st=%.1f", start)
}

func DurationArg(duration float64) string {
	return fmt.Sprintf("d=%.1f", duration)
}

func ColorArg(color string) string {
	return fmt.Sprintf("c=%s", color)
}

func buildFilter(options Options) string {
	fadeTypeArg := FadeTypeArg(options.FadeType)
	startArg := StartArg(options.Start)
	durationArg := DurationArg(options.Duration)
	colorArg := ColorArg(options.Color)
	return fmt.Sprintf(
		"fade=%s:%s:%s:%s",
		fadeTypeArg,
		startArg,
		durationArg,
		colorArg,
	)
}
