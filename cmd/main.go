package main

import (
	"flag"
	"log"
	"os"
	"os/exec"

	"gitlab.com/dak425/fade"
)

func renderFade(ffmpegPath, input string, output string, options fade.Options) {
	args := []string{
		"-i",
		input,
		"-vf",
		options.String(),
		"-y",
		output,
	}
	log.Print("Rendering video with fade options...")
	err := exec.Command(ffmpegPath, args...).Run()
	if err != nil {
		log.Fatalf("Error when rendering video: %s", err)
	}
	log.Println("Video rendering complete!")
	os.Exit(0)
}

func previewFade(ffplayPath, input string, options fade.Options) {
	args := []string{
		"-i",
		input,
		"-vf",
		options.String(),
	}
	log.Printf("Previewing with: %v\n", args)
	err := exec.Command(ffplayPath, args...).Run()
	if err != nil {
		log.Fatalf("Error when previewing fade: %s", err)
	}
	os.Exit(0)
}

func main() {
	ffmpegPath, err := exec.LookPath("ffmpeg")
	if err != nil {
		log.Fatal("ffmpeg not found in PATH, install ffmpeg or add it to your PATH variable")
	}
	ffplayPath, err := exec.LookPath("ffplay")
	if err != nil {
		log.Fatal("ffplay nott found in PATH, install ffplay or add it to your PATH variable")
	}

	var input, output, fadeType, color string
	var start, duration float64
	var help, render bool

	flag.StringVar(&input, "input", "", "File path to the video that will have the text rendered on it. Required")
	flag.StringVar(&output, "output", "", "File path to write the rendered video to. Required if rendering")
	flag.StringVar(&fadeType, "type", "in", "The type of fade effect to apply, options are 'in' and 'out'. Default is 'in'")
	flag.StringVar(&color, "color", "black", "The color to fade in or out to. Default is 'black'")
	flag.Float64Var(&start, "start", 0, "The time in seconds to start the fade effect. Default is 0")
	flag.Float64Var(&duration, "duration", 0, "The time in seconds for the fade effect to last. Default is 0")
	flag.BoolVar(&render, "render", false, "Enables writing the video out to a file. Default is false")
	flag.BoolVar(&help, "help", false, "Displays the usage information")

	flag.Parse()

	if help {
		flag.PrintDefaults()
		os.Exit(0)
	}

	if input == "" {
		log.Fatalf("Input path is required")
	}

	options := fade.NewOptions()
	options.FadeType = fadeType
	options.Start = start
	options.Duration = duration
	options.Color = color

	if render {
		renderFade(ffmpegPath, input, output, options)
	}
	previewFade(ffplayPath, input, options)
}
